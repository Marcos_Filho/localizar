package com.example.localizar.activitys;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.util.Log;

import com.example.localizar.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    Float Latitude, Longitude;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Bundle bundle = getIntent().getExtras();
        Latitude = Float.parseFloat(bundle.getString("latitude").toString());
        Longitude = Float.parseFloat(bundle.getString("longitude").toString());
        Log.d("TAGGG", "onCreate: "+ Latitude + "   "+ Longitude);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng local = new LatLng(Latitude, Longitude);
        mMap.addMarker(new MarkerOptions().position(local).title(Latitude+" / "+Longitude));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(local));
    }
}